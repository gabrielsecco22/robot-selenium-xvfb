*** Settings ***
Documentation     This is a basic test
Library           Selenium2Library
Library           BuiltIn

*** Variables ***

*** Test Cases ***
User can open home page
    [Documentation]                 As a user I can open the home page and see the tagline
    COMMENT                         Tests

    open browser                    http://www.airbnb.co.nz     chrome
    wait until page contains        Book
    close browser

Browser can be opened
    [Documentation]                 As a automated test i can open the browser

    open browser                    http://www.airbnb.co.nz     chrome
    sleep                           10s
    close browser

Basic assert
    [Documentation]                 As a automated test i assert basic things

    should be equal                 10  10

*** Keywords ***
